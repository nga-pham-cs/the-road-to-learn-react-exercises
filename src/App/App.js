import React, {Component} from 'react';
import './App.css';
// For fetching data from an API
import {
    DEFAULT_QUERY,
    PATH_BASE,
    PATH_SEARCH,
    PARAM_SEARCH
} from '../Constants';

import {
    Table
} from "../Table";
import {Search} from "../Search";

const url = `${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${DEFAULT_QUERY}`;

function isSearched(searchTerm) {
    // Return another function, because input type of filter function is 'function'
    return function (item) {
        return item.title.toLowerCase().includes(searchTerm.toLowerCase());
    }
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: null,
            searchTerm: DEFAULT_QUERY,
            isLoaded: false,
            error: null,
        };

        // Bind functions in the constructor, to define it as class method
        // this.setSearchTopStories = this.setSearchTopStories.bind(this);
        this.onDismiss = this.onDismiss.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        // For changing searchTerm
        this.onSearchSubmit = this.onSearchSubmit.bind(this);
        this.fetchSearchTopStories = this.fetchSearchTopStories.bind(this);
    }

    setSearchTopStories(result) {
        this.setState({result});
    }

    onSearchSubmit(event) {
        const {searchTerm} = this.state;
        this.fetchSearchTopStories(searchTerm);
        event.preventDefault();
    }

    fetchSearchTopStories(searchTerm) {
        let url = `${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}`;
        fetch(url).then(response => response.json())
            .then(result => this.setSearchTopStories(result))
            .catch(error => this.setState({error}));
    }

    // Do asynchronous request to fetch data from an API
    componentDidMount() {
        const {searchTerm} = this.state;
        this.fetchSearchTopStories(searchTerm);
    }

    // Define onDismiss's functionality, the business logic, in the class
    onDismiss(id) {
        // Remove item with specified id
        const updatedList = this.state.list.filter(item => item.objectID !== id);
        // Update state
        this.setState({
            result: {...this.state.result, hits: updatedList}

        });
    }

    onSearchChange(event) {
        this.setState({searchTerm: event.target.value});
    }

    render() {
        // Destructuring technique
        const {searchTerm, isLoaded, result, error} = this.state;
        /*if (!isLoaded) {
            return (
                <div>Loading...</div>
            )
        }*/
        if (error) {
            return (
                <p>Sorry, something went wrong. Please try again later.</p>
            )
        }

        // if (!result) { return (<p>Sorry, no result found</p>); }
        return (
            <div className="page">
                <div className="interactions">
                    <Search
                        value={searchTerm}
                        onChange={this.onSearchChange}
                        onSubmit={this.onSearchSubmit}
                    >Search
                    </Search>
                </div>
                {result &&
                <Table
                    list={result.hits}
                    onDismiss={this.onDismiss}
                />}
            </div>
        );
    }
}

export default App;

