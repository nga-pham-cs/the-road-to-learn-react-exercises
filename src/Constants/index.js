// Change URL endpoint for the Hacker New API ('The road to learn react' exercises)
const DEFAULT_QUERY = 'redux';
const PATH_BASE = 'https://hn.algolia.com/api/v1';
const PATH_SEARCH = '/search';
const PARAM_SEARCH = 'query=';
const PAGE = '';
const PAGE_NUMBER = 3;
export {
    DEFAULT_QUERY,
    PATH_BASE,
    PATH_SEARCH,
    PARAM_SEARCH
}