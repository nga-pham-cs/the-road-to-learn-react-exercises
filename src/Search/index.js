import React from 'react';

// Change from class to functional stateless component; because access to local state is not necessary
export const Search = ({value, onChange, onSubmit, children}) => {
    // Explicit definition of return() function should be used for future use
    return (
        <form>
            {children}
            <input type="text"
                   value={value}
                   onChange={onChange}
            />
            <button type="submit">{children}</button>
        </form>
    );
}