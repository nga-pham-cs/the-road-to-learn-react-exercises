import React from 'react';

import {
    Button
} from "../Button";


// For styling table rows
const largeColumn = {
    width: '40%',
};
const midColumn = {
    width: '30%',
};
const smallColumn = {
    width: '10%',
};

// Change from class component into functional stateless component
export const Table = ({list, onDismiss}) => {
    return (
        <div className="table">
            {list.map(item => {
                const onHandleDismiss = () => {
                    this.onDismiss(item.objectID);
                }
                return (
                    <div key={item.id} className="table-row">
                        <span style={midColumn}>{item.title}</span>
                        <span style={largeColumn}>{item.url}</span>
                        <span style={smallColumn}>{item.author}</span>
                        <span style={smallColumn}>
                                <Button
                                    className="button-inline"
                                    // Inlined Javascript ES6 arrow function
                                    //onClick={() => this.onDismiss(item.objectID)}
                                    // An alternative way to handle event: Define wrapping function outside the method,
                                    // to pass only the defined function to the handler
                                    onClick={onHandleDismiss}
                                    type="button"
                                >
                                Dismiss
                                </Button>
                            </span>
                    </div>
                )
            })}

        </div>
    )
}