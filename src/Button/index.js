import React from 'react';

// Change from class component into functional stateless component
export const Button = ({onClick,
                    className = '',
                    children}) => {
    return (
        <button
            onClick={onClick}
            className={className}
            type="button"
        >
            {children}
        </button>
    )
}